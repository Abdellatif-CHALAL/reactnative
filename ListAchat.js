import React, { useContext, useState } from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar, Button, TouchableHighlight, Icon } from 'react-native';

const Item = ({ name }) => (
    <View style={styles.item}>
        <Text style={styles.name}>{name}</Text>
    </View>
);

const ListAchats = () => {

    const [data, setData] = useState([
        {
            id: 1,
            name: 'First Item',
            status: "confirmed"
        },
        {
            id: 2,
            name: 'Second Item',
            status: "confirmed"
        },
        {
            id: 3,
            name: 'Third Item',
            status: "confirmed"
        },
        {
            id: 3,
            name: 'Third Item',
            status: "confirmed"
        },
    ]);

    useEffect(() => {
        getData();
    }, []);

    const renderItem = ({ item }) => (
        <Item name={item.name} />
    );

    async function getData() {
        const result = await AsyncStorage.getItem("listAchat");
        const values = result != null ? JSON.parse(result) : [];
        setData(values);
    }

    const addItem = async (value) => {
        const items = [...data, value];
        try {
            await AsyncStorage.setItem(
                "listAchat",
                JSON.stringify(items),
            );
            setData(items);
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <SafeAreaView style={styles.container}>
            <TouchableHighlight onPress={addItem}>
                <View style={styles.button}>
                    <Text>Add item</Text>
                </View>
            </TouchableHighlight>
            <FlatList
                data={data}
                renderItem={renderItem}
                keyExtractor={item => item.id}
            />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: 'white',
        padding: 10,
        marginVertical: 6,
        marginHorizontal: 10,
    },
    name: {
        fontSize: 22,
    },
    button: {
        margin: 10,
        alignItems: "center",
        backgroundColor: "green",
        padding: 16
    },
});

export default ListAchats;
